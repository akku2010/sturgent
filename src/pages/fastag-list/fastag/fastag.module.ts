import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FastagPage } from './fastag';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FastagPage,
  ],
  imports: [
    IonicPageModule.forChild(FastagPage),
    TranslateModule.forChild()
  ],
})
export class FastagPageModule {}
